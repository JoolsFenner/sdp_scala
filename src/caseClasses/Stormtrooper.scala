package caseClasses

object Stormtrooper {
  def inspect(person: Person): String = {
	person match{
	  case Person("Luke", "Skywalker") => 
	    "Stop, rebel, scum!"
	  case Person("Dave","Grohl") =>
	    "Hello, god like person"
	  //matching on type below
	  case Person(first,_) =>
	    s"Move along $first"
	}
    
//    if (person.firstName == new Person("Luke", "Skywalker"))
//      "Stop rebel scum!"
//    else if  
    
  }
}

object Tester{
  
  def main(args: Array[String]): Unit ={
    println(Stormtrooper.inspect(Person("Dave", "Grohl")))
    println(Stormtrooper.inspect(Person("Julian", "Fenner")))
  }
}