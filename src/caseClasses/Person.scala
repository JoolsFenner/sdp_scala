package caseClasses

/*
 * case class based on pattern matching
 * http://docs.scala-lang.org/tutorials/tour/case-classes.html
 * 
 * val fields can be removed with case
 */

case class Person (firstName: String, lastName: String){
  def name = firstName + " " + lastName
}

object Test{
  def main(args: Array[String]) = {
    val p = new Person("Julian", "Fenner")
    val p1 = new Person("Julian", "Fenner")
    val p2 = p1.copy()
    //try all of the below with and without "case" class
    println(p)
    println(p1)
    println((p.equals(p1)))
    println(p == p1)
    println(p eq p1)
    println(p1 equals p2)
  }
  
}