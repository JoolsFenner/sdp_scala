package intlist

/*
 * Linked list definition
 */

sealed trait IntList {
  
  import scala.annotation.tailrec
  // annotation to tell whether tail recursive 
  //@tailrec  
  def length: Int =
    this match {
      case EndOfList => 0
      case Node(hd, tl) => 1 + tl.length
    }
  
  def double: IntList =
    this match {
      case EndOfList => EndOfList
      case Node(hd,tl) => Node(hd * 2, tl.double)
  }
  
  def sum:Int =
    this match {
      case EndOfList => 0
      case Node(hd,tl) => hd + tl.sum
  }
  
  //abstracted versions of above i.e foldleft
  
 def abstraction(end:Int, f: (Int,Int) => Int):Int = 
      this match {
        case EndOfList=> end
        case Node(head, tail) => f(head, tail.abstraction(end, f))
    }
  
 
def sum2:Int =  abstraction(0, (head, tail) => head + tail)
      
def length2:Int =  abstraction(0, (head, tail) => 1 + tail)

def product2 :Int = abstraction(1, (head, tail) => head * tail)
    
  }
  
  


final case object EndOfList extends IntList
final case class Node(head: Int, tail: IntList) extends IntList








object Thing extends App {
  val l = Node(1, Node(2, Node(3, Node(4, EndOfList))))
  println(l.sum)
  println(l.length)
  println("hello")
  

}