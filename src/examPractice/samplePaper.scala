package examPractice

import scala.math._


object samplePaper extends App {
	
	val a = 10.0
	val b = estimateB(a, 0, a, .000000001)
	println("Estimate is " + (a/b))

 
	def estimateB(	a: Double, 
					bMin: Double,
					bMax: Double,
					err: Double) : Double = {
		
	  
	  
		val b = (bMax - bMin) / 2
		val left = ((a+b)/a)
		val right = a/b
		val difference = abs(left - right )
		
		println("b " + b)
		if(difference < err) return b
	
		if (left > right){
		    val bool = b > bMin
			//println("left " + bool)
		  estimateB(a, bMin,b,err)
			}
		else { 
		  val bool = b < bMax
//			println("right " + bool)
			estimateB(a,b,bMax,err)			
		}
					
	}
  
}