package examPractice

object SamplePaper7 extends App {

  val x = List(List('a, 'b, 'c), List('d, 'e), List('f, 'g, 'h), List('d, 'e), List('i, 'j, 'k, 'l), List('m, 'n), List('o))
  
  println(lsort(x))
  def lsort[A](ls: List[List[A]]): List[List[A]] =
    //ls.sortWith{_.length < _.length}
    ls.sortWith((x,y) => x.length < x.length)
}
