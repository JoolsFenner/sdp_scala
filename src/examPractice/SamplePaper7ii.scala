package examPractice

object SamplePaper7ii extends App{

  def lsort[A](ls: List[List[A]]): List[List[A]] =
    ls.sortWith{ (x,y) => x.length < y.length}
  
  
 
  val lst = List(List('a, 'b, 'c), List('d, 'e), List('f, 'g, 'h), List('d, 'e), List('i, 'j, 'k, 'l), List('m, 'n), List('o))
  
  
  println(lsort(lst))
  
}