package examPractice

object mockPaper extends App {

  val pieKinds = List("Stewed Eels", "Jellied Eels", "only beef", "no mash or liquor")

  //a
  val pieGen = new Generator[Pie] {
    def generate: Pie = Pie(this.oneOf(pieKinds).generate) } 

  //val pieGen2: Generator[Pie] = oneOf[Pie](pieKinds.map { s:String => Pie(s) })
  
  }
 


case class Pie(kind: String)



trait Generator[T] {
  self =>
  // an alias for this.
  def generate: T
  
  def oneOf[T](ls: List[T]): Generator[T] = choose(0, ls.length -1).map{x => ls(x)}
  
  def flatMap[S](f: T => Generator[S]): Generator[S] = new Generator[S] {
    def generate = f(self.generate).generate
  }
  
  def map[S](f: T => S): Generator[S] = new Generator[S] {
  
    def generate = f(self.generate)
  }
  
  
  implicit def integers: Generator[Int] = new Generator[Int] {
    def generate = scala.util.Random.nextInt()
  }
  
  implicit def choose(lo: Int, hi: Int): Generator[Int] = new Generator[Int] {
    def generate = scala.util.Random.nextInt(hi - lo) + lo
  }
  
  def positives: Generator[Int] = integers.map{x => if(x < 0) -x else x}
  
  
  
}