package examPractice

object SamplePaper5 extends App{


def createFractal(n: Int): List[String] = 
    createFractal(n, List(" |","/|"))
  
  def createFractal(n: Int, lst: List[String]): List[String] =  
  if(n == 1) 
    lst
   else {
     //almost right but need to double white space of each element in first lst every iteration
     // have to concat lst here as if done as argument of recursive call the lst ::: lst.map(...) 
     // method will concat lsts with different values of n 
     val newLst =  lst ::: lst.map(x => x + x)
     createFractal( n - 1,  newLst )
   }
   
  println(createFractal(4).mkString("\n"))
   
  def getWhiteSpace(n: Int): String = n match {
  case 1 => " "
  case _ => " " + getWhiteSpace(n-1)
  
}

   

 
 
}