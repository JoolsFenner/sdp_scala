package examPractice

object examtest {
 
 val lst = List(List('a), List('a, 'a, 'a), List('a,'a,'a,'a,'a), List('a,'a,'a,'a,'a,'a,'a))
                                                  //> lst  : List[List[Symbol]] = List(List('a), List('a, 'a, 'a), List('a, 'a, 'a
                                                  //| , 'a, 'a), List('a, 'a, 'a, 'a, 'a, 'a, 'a))
 val x::y = List(List('a), List('a, 'a, 'a), List('a,'a,'a,'a,'a), List('a,'a,'a,'a,'a,'a,'a))
                                                  //> x  : List[Symbol] = List('a)
                                                  //| y  : List[List[Symbol]] = List(List('a, 'a, 'a), List('a, 'a, 'a, 'a, 'a), L
                                                  //| ist('a, 'a, 'a, 'a, 'a, 'a, 'a))
 val z = x:::y                                    //> z  : List[java.io.Serializable] = List('a, List('a, 'a, 'a), List('a, 'a, 'a
                                                  //| , 'a, 'a), List('a, 'a, 'a, 'a, 'a, 'a, 'a))
 
 
 }