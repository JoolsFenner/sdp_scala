package examPractice

object ScalaPaper6 extends App {

  val x = MyBigInt(39)
 // println(x.factor)

  x.enumerateFactors()
}

case class MyBigInt(x: Int){
  
  
  
  
  def factor: MyBigInt = {
    
    def factor(y: MyBigInt):MyBigInt = {
      
      if(divideBigIntegers(MyBigInt(2)).isLessThan(y))
          MyBigInt(1)
      else if (modulusBigIntegers(y).isEqualTo(MyBigInt(0)))
        y
      else factor(y.addBigIntegers(MyBigInt(1)))
    }
    
    factor(MyBigInt(2))
    
  }
  
  
  def enumerateFactors() : Unit = {
    
    def enumerateFactors(y: MyBigInt): Unit = {  
     
      if(divideBigIntegers(MyBigInt(2)).isGreaterThan(y)){
         if(y.factor.isEqualTo(MyBigInt(1)) && modulusBigIntegers(y).isEqualTo(MyBigInt(0))) {
           println(y)
           }
         enumerateFactors(y.addBigIntegers(MyBigInt(1)))
         }
       }
    
    enumerateFactors(MyBigInt(2))
  }
  
  
  def modulusBigIntegers(x: MyBigInt): MyBigInt = {
    MyBigInt(this.x % x.x)
  }
  
  def isEqualTo(x: MyBigInt): Boolean = {
    this.x.equals(x.x)
    
  }
  def addBigIntegers(x: MyBigInt): MyBigInt = {
    MyBigInt(this.x + x.x)
  }
  def divideBigIntegers(x: MyBigInt): MyBigInt = {
    MyBigInt(this.x / x.x)
  }
  
  def isLessThan(x: MyBigInt): Boolean = {
     this.x < x.x
  }
  
   def isGreaterThan(x: MyBigInt): Boolean = {
     this.x > x.x
  }
  
}