package examPractice

import scala.math._

object SamplePaper3 extends App {
  
  def estimateB(a: Double, bMin: Double, bMax: Double, err: Double) : Double = {
    val b = (bMax - bMin) / 2
    val left = (a + b) / a
    val right = a / b
    val diff = abs(left - right)
    
    println(diff)
    
//    println("bmin" + bMin)
//    println("bmax" + bMax)
    
    if(diff < err)
      b
    else
      if(left > right){
        println("left > right")
        //return diff
        estimateB(a, bMin, b, err)
      }else{ //left < right
        println("right > left")
        //return diff
        estimateB(a, b, bMax, err)
        }
  }
  
  val b = estimateB(4, 0, 4, .000000001)
}