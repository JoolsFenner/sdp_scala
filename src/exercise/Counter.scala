package exercise

class Counter(
    val count: Int) {
  
	def inc: Counter = inc() // means you dont have to use empty parentheses for default value
	def inc(x:Int = 1) = new Counter(count + x)
	
	def dec: Counter = dec()
	def dec(x:Int =1 ) = new Counter(count - x)
	
	def adjust(adder: Adder) = new Counter(adder.add(count))
}


class Adder (amount: Int){
   def add(in: Int) = in + amount
   def apply(in: Int) = in + amount;
}


object TestCounter {

  def main(args: Array[String]): Unit = {
	//typical example of functional Scala
	//println(new Counter(10).inc.count )
	//println(new Counter(10).inc(3).count)
	//println(new Counter(10).inc.inc.dec.count)
    //println(new Counter(10).inc(9).count)
	
	println(new Adder(5).add(2))
	
	val item = new Adder(5)
	item(2) // item apply(2)
  }
 }