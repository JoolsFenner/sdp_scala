package helloworld

object First {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  1 + 1                                           //> res0: Int(2) = 2
  7.min(6).min(5)                                 //> res1: Int = 5
  if(10 > 14) "left" else "right"                 //> res2: String = right
  if(10 > 14) "left"                              //> res3: Any = ()
  println("str and something = " + "something else = " + 34)
                                                  //> str and something = something else = 34
  
  
  assert(square(2.0) == 4.0)
  assert(square(3.0) == 9.0)
  
  /*
  def square(in: Double): Double =
  ???  //three question marks together very useful for testing the assert statement without implementation.  Just checks whether it returns the right type
  */
  
  def square(in: Double): Double = in * in        //> square: (in: Double)Double
  
 	def greeting(firstName: String = "Some", lastName: String = "Body") =
 	"hello " + firstName + " " + lastName + "!"
                                                  //> greeting: (firstName: String, lastName: String)String
                                                  
   greeting("Fred")                               //> res4: String = hello Fred Body!
 	 greeting(lastName = "Dude")              //> res5: String = hello Some Dude!
 	   
}