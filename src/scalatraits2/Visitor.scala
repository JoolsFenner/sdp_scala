package scalatraits2

import java.util.Date

/*
 * Trait is Scala's interface
 * 
 * UAP - Uniform access principle - don't care if its method of field
 * 
 * Sealed A sealed class may not be directly inherited, except if the 
 * inheriting template is defined in the same source file as the inherited class. 
 * However, subclasses of a sealed class can inherited anywhere.
 */


sealed trait Visitor{
  def id: String
  def createdAt: Date
  def age: Long = new Date().getTime() - createdAt.getTime()
}

// case class gives you.... hashcode, tostring, apply, unapply etc

final case class Anonymous(id: String, 
					 createdAt: Date
					 ) extends Visitor

final case class User(id: String,
				email: String,
				createdAt: Date = new Date()
				) extends Visitor

	
object Thing extends App{
 // because Sealed used must define all the cases or compiler will complain.  Strong type checking
  def missing(v: Visitor) = 
   v match {
   	case User(_,_,_) => "ah a user"
     
 }
  
}				
				
				
//case class Anonymous (
//    id: String, 
//    createdAt: Date = new Date()) extends Visitor
//
//case class User (
//    id: String, 
//    email: String, 
//    createdAt: Date = new Date()) extends Visitor {
//  override val age: Long = 15
//}
//
//object Test{
//  def main(args: Array[String]): Unit = {
//    println(Anonymous("anon1"))
//    println(Anonymous("anon2").age)
//  }
// }