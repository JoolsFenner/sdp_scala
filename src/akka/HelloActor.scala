//package akka
//
//import akka.actor.{Props, ActorSystem, Actor}
//
//class HelloActor extends Actor{
//  
//  override def receive = {
//    case "hello" => println("hello back at you")
//    case _ => print("eh?")
//  }
//  
//
//}
//
//
//
//object MainHelloActor extends App {
//  val system = ActorSystem("HelloSystem")
//  val helloActor = system.actorOf(Props[HelloActor], name = "helloActor")
//  //val helloActor = system.actorOf( Props(new HelloActor("Fred")), name = "helloActor") // alternative
//  helloActor ! "hello"
//  helloActor ! "goodbye"
//}