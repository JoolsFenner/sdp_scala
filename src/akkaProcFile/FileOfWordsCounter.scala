//package akkaProcFile
//
//import akka.actor.Actor
//import akka.actor.ActorRef
//import akka.actor.Props
//import akka.actor.ActorSystem
//
//import akka.pattern.ask
//import scala.concurrent.duration._
//import scala.concurrent.ExecutionContext.Implicits.global 
//import akka.util.Timeout
//
//case class StartProcessingFile()
//case class ProcessLineMsg(line: String)
//case class ProcessedLine(word: Int)
//
//class FileOfWordsCounter(fileName:String) extends Actor {
//  private var running = false
//  private var fileSender: Option[ActorRef] = None
//  private var lineCounter = 0
//  private var runningTotal = 0
//  private var linesProcessed = 0
//  
//  def receive = {
//    case StartProcessingFile =>
//      if(running){
//        println("...already running")
//      } else{
//        println("started running ...")
//        running = true
//        fileSender = Some(sender) //save ref to parent process
//    
//	    import scala.io.Source._
//	    fromFile(fileName).getLines().foreach{
//	      line => { 
//	        context.actorOf(Props[WordCounterActor], "wcactor" + lineCounter) ! ProcessLineMsg(line)
//	        println(s"starting actor " + "wcactor" + lineCounter)
//	        lineCounter += 1
//	      }
//         }
//        } 
//    case ProcessedLine(words) =>
//      runningTotal += words
//      linesProcessed += 1
//      if(linesProcessed == lineCounter){
//        fileSender.map(_! runningTotal)
//      }
//    case _ => println("asdfghj")
//      
//   }
//  
//  
//}
//
//
//class WordCounterActor extends Actor {
//  override def receive = {
//    case ProcessLineMsg(line) => 
//      val wordInLine = line.split(" ").length
//      sender ! ProcessedLine(wordInLine)
//    case _ => println("aaaah")
//    
//  }
//  
//}
//
//object LineCounter extends App {
//  val system = ActorSystem("wordCountSystem")
//  val actor = system.actorOf(Props(new FileOfWordsCounter(args(0))), "filewcactors")
//  
//  implicit val timeout = Timeout(30 seconds)
//  
//  val future = actor ? StartProcessingFile // ? is 'ask'
//  future.map { result =>
//    println("Total number of words is " + result)
//    system.shutdown()
//    }
//  
//}