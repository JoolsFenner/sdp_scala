package feb26

object Thing extends App {

  /*
   * Call by name - Arguments recomputed every time
   * http://stackoverflow.com/questions/13337338/call-by-name-vs-call-by-value-in-scala-clarification-needed
   * 
   */
  
  var count = 1
  xxx(count <= 5){
    println(s"value $count")
    count += 1
  }
  
  
  def xxx(conditional: => Boolean)(body: => Unit): Unit ={
    if(conditional){
      body
      xxx(conditional)(body)
    }
  }
  
  def yyy(i: Int)(j: Int): Int = i + j
  
  println(yyy(3)(4))
  
}