package feb26

object Currying extends App{
  
  def cat1(s1: String)(s2:String) = s1 + s2

  def cat2(s1: String) = (s2: String) => s1 + s2
  
//  println(cat1("hello ")("world"))
//  println(cat2("hello ")) // returns <function1> (means function taking 1 arg)
  
  val f = cat2("Boris ")
  val s  = f("the spider")
//  println(s)
  
  
  def addn(n: Int) = (i: Int) => i + n
  
  val add5 = addn(5) //argument being 'frozen' here, this is currying
  
  println(add5(10))
}