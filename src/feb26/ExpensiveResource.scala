package feb26

object ExpensiveResource extends App{

	lazy val resource: Int = init()
	
	def init(): Int = {
	  //do something expensive
	  println("ouch")
	  0
	}
  
	//try without 'lazy' and below commented
	println(resource)
}