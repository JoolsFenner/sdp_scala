package revision

object RevisionClass070515_ii {
 


		
}

class MyList[T]{


//def myFilter(fn: T => Boolean): MyList[T] = this match{

	//case Nil => this
	//case hd::tl => if(fn(hd)) hd :: tl.myFilter(fn)
		//							else tl.filter(fn)
	//}

	def sum(ls: List[Int]):Int = ls match {
		case Nil => 0
		case hd::tl => hd + sum(tl)
	}


		def product(ls: List[Int]):Int = ls match {
			case Nil => 1
			case hd::tl => hd * product(tl)
		}
		
		def sum3(ls: List[Int]): Int = (0::ls) reduceLeft (_ + _)
		def product2(ls: List[Int]): Int = (0::ls) reduceLeft (_ * _)
		
		def reduce(fn: (T,T) => T): T = this match{
			case Nil =>  //throw new Error("error")
			case hd::tl => (tl fold hd) (fn)
		}
		
}