package revision

object KeySyntax3_HigherOrderF {
  
  val a = List(1,2,3,4,5,6,7,8,9,10)              //> a  : List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
  
 
  /**
  * Fold
  */
  a.foldLeft(0){ (x,y) => x + y }                 //> res0: Int = 55
  a.foldLeft(0)(_ + _)                            //> res1: Int = 55
  
  a.foldLeft(List[Int]()) { (acc,i) => i :: acc } //> res2: List[Int] = List(10, 9, 8, 7, 6, 5, 4, 3, 2, 1)
  
  
  /**
  * Map
  */
  a.map( x => x * x)                              //> res3: List[Int] = List(1, 4, 9, 16, 25, 36, 49, 64, 81, 100)
  a.map(3 *_)                                     //> res4: List[Int] = List(3, 6, 9, 12, 15, 18, 21, 24, 27, 30)
  
  (1 to 3).map("*" * _).foreach(println _)        //> *
                                                  //| **
                                                  //| ***
  
  
  /**
  * Flatmap
  */
  val nameList = List("Julian", "Linda", "Andrea")//> nameList  : List[String] = List(Julian, Linda, Andrea)
  def ulcase(str:String):List[String] = List(str.toLowerCase(), str.toUpperCase())
                                                  //> ulcase: (str: String)List[String]
  
  nameList.map(ulcase)                            //> res5: List[List[String]] = List(List(julian, JULIAN), List(linda, LINDA), Li
                                                  //| st(andrea, ANDREA))
  nameList.flatMap(ulcase)                        //> res6: List[String] = List(julian, JULIAN, linda, LINDA, andrea, ANDREA)
  
  
  /**
  * Filter
  */
  a.filter(x => x % 3 == 0)                       //> res7: List[Int] = List(3, 6, 9)
  a.filter(_ % 3 == 0)                            //> res8: List[Int] = List(3, 6, 9)
  
  
  /**
  * Reduce
  */
  a.reduce( (x,y) => x * y)                       //> res9: Int = 3628800
  a.reduce(_ * _)                                 //> res10: Int = 3628800
  
  /**
  * Zip
  */
  val x = List(6,7,8)                             //> x  : List[Int] = List(6, 7, 8)
  val y = List("d","e","f")                       //> y  : List[String] = List(d, e, f)
  
  x.zip(y)                                        //> res11: List[(Int, String)] = List((6,d), (7,e), (8,f))
  
  
  /**
  * Collect
  *
  */
 	
 	val myList = List(2,"apple", 4 ,"pear",18,27,"zebra")
                                                  //> myList  : List[Any] = List(2, apple, 4, pear, 18, 27, zebra)
 	
 	myList.collect{case x: Int => x * x
 								 case x: String => x.toUpperCase()}
                                                  //> res12: List[Any] = List(4, APPLE, 16, PEAR, 324, 729, ZEBRA)
 	
 	
 	
 	
 	
}