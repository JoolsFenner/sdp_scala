package revision

object Revision140514 {
  def sum3(ls: List[Int]): Int = (0::ls) reduceLeft (_ + _)
                                                  //> sum3: (ls: List[Int])Int
 
 sum3(List(1,2,3,4,5))                            //> res0: Int = 15
 val a = Array[Int](1,2,3,4,5,6,7,8)              //> a  : Array[Int] = Array(1, 2, 3, 4, 5, 6, 7, 8)
 val b: Array[Int] = Array(1,2,3)                 //> b  : Array[Int] = Array(1, 2, 3)
 val c = Array[Int](1,2,3)                        //> c  : Array[Int] = Array(1, 2, 3)
 
 
 a map(_ *2) filter(_ > 5)                        //> res1: Array[Int] = Array(6, 8, 10, 12, 14, 16)
 
 val r: Range = 1 until 10 by 3                   //> r  : Range = Range(1, 4, 7)
 
 //CARTESIAN PRODUCT
 //Uses vector because it goes for 'cheapest' structure
 (1 to 5) map(x => (4 to 8) map ( y => (x,y)))    //> res2: scala.collection.immutable.IndexedSeq[scala.collection.immutable.Index
                                                  //| edSeq[(Int, Int)]] = Vector(Vector((1,4), (1,5), (1,6), (1,7), (1,8)), Vecto
                                                  //| r((2,4), (2,5), (2,6), (2,7), (2,8)), Vector((3,4), (3,5), (3,6), (3,7), (3,
                                                  //| 8)), Vector((4,4), (4,5), (4,6), (4,7), (4,8)), Vector((5,4), (5,5), (5,6), 
                                                  //| (5,7), (5,8)))
                                                  
 (1 to 5) flatMap(x => (4 to 8) map ( y => (x,y)))//> res3: scala.collection.immutable.IndexedSeq[(Int, Int)] = Vector((1,4), (1,5
                                                  //| ), (1,6), (1,7), (1,8), (2,4), (2,5), (2,6), (2,7), (2,8), (3,4), (3,5), (3,
                                                  //| 6), (3,7), (3,8), (4,4), (4,5), (4,6), (4,7), (4,8), (5,4), (5,5), (5,6), (5
                                                  //| ,7), (5,8))

def scalarProduct(x: Vector[Double], y: Vector[Double]): Double =
//	(x zip y) map (xy => xy._1 * xy._2) sum
		(x zip y) map {case (x,y) => x * y} sum
                                                  //> scalarProduct: (x: Vector[Double], y: Vector[Double])Double

scalarProduct(Vector(1,2,3,4),Vector(4,5,6))      //> res4: Double = 32.0
}