package revision

object Curry extends App{
  
  def squareIf(f: Int => Boolean)(a:Int):Int =
		  if(f(a)) 
		    a * a
		    else
		      a
		      
		      
 val squareIfEven = squareIf(x => x % 2 == 0 )_
 
 println(squareIfEven(3))
 println(squareIfEven(4))
 
 
 val x = squareIf(x => x % 2 == 0 )(4)
 
}