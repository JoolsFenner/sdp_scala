package revision
import scala.annotation.tailrec



object KeyAlgorithms extends App {
 
  /**
   * Tail Recursion
   */
  //NON TR
 // @tailrec
  def factorial(n: Int): Int = 
    if (n == 0) 1 else n * factorial(n - 1)
  
  
  //TR
  def facTr(x:Int): Int = {
   @tailrec
    def facTrAcc(x: Int, acc: Int):Int = x match{
      case 0 => acc
      case _ => facTrAcc(x-1, acc * x)
    }
    facTrAcc(x, 1)
  }
  
  
  //println(factorial(50))
  
  /**
   * Mymap - Generic
   * 
   */
  
  def myMap[T](f: T => T, x: List[T]): List[T] =  x match {
    case Nil => Nil
    case hd::tl => f(hd) :: myMap(f, tl)
  }

  myMap( (x: Int) => x * x, List(2,3,4))
  myMap( (x: String) => x + "!", List("hello", "how", "are", "you"))
  
  
  
  
  
  
  
}