package revision

object Lists {
  val mixedList1 = List("a", 1, 2, "b", 19, 42.0, List(1)) //this is a List[Any]
                                                  //> mixedList1  : List[Any] = List(a, 1, 2, b, 19, 42.0, List(1))
	val results = mixedList1 collect {
	  case s: String => "String:" + s
	  case i: Int => "Int:" + i.toString
	  case d: Double => "Double:"  + d.toString
	  case l: List[_] => "List Int" + l.toString
	}                                         //> results  : List[String] = List(String:a, Int:1, Int:2, String:b, Int:19, Dou
                                                  //| ble:42.0, List IntList(1))
     
     
     
     
    /*
    LISTS
    */
    List(1, 2) :+ 3                               //> res0: List[Int] = List(1, 2, 3)
    3 +: List(1, 2)                               //> res1: List[Int] = List(3, 1, 2)
    
    1 :: 2:: List(3, 4)                           //> res2: List[Int] = List(1, 2, 3, 4)
                                                  
    1 :: List(2,3)                                //> res3: List[Int] = List(1, 2, 3)
    List(1) :: List(2,3)                          //> res4: List[Any] = List(List(1), 2, 3)
    List(1) ::: List(2,3)                         //> res5: List[Int] = List(1, 2, 3)
  	
  	//PARTITION - puts all true element in one list and all others in second
  	List(2,4,5,1,5,6,0,10) partition(_< 5)    //> res6: (List[Int], List[Int]) = (List(2, 4, 1, 0),List(5, 5, 6, 10))
  	//vs
  	// SPAN - Puts all element in one list until an element is false.
  	// From that point on it puts elements in second list
  	List(2,4,5,1,5,6,0,10) span (_ < 5)       //> res7: (List[Int], List[Int]) = (List(2, 4),List(5, 1, 5, 6, 0, 10))
  	
  	val (odd, even) = List(1,2,3,4,5,6) partition (_ % 2 ==0)
                                                  //> odd  : List[Int] = List(2, 4, 6)
                                                  //| even  : List[Int] = List(1, 3, 5)
  	odd                                       //> res8: List[Int] = List(2, 4, 6)
  	even                                      //> res9: List[Int] = List(1, 3, 5)
  	/*
  	SETS
  	*/
   	val a = Set(1,2,3,4,5)                    //> a  : scala.collection.immutable.Set[Int] = Set(5, 1, 2, 3, 4)
   	val aa= Set(1,2)                          //> aa  : scala.collection.immutable.Set[Int] = Set(1, 2)
 	  val b = Set(3,4,5,6,7)                  //> b  : scala.collection.immutable.Set[Int] = Set(5, 6, 7, 3, 4)
   	a union b                                 //> res10: scala.collection.immutable.Set[Int] = Set(5, 1, 6, 2, 7, 3, 4)
		a contains 1                      //> res11: Boolean = true
		a contains 10                     //> res12: Boolean = false
  	aa subsetOf a                             //> res13: Boolean = true
  	a ++ b                                    //> res14: scala.collection.immutable.Set[Int] = Set(5, 1, 6, 2, 7, 3, 4)
  	a -- b                                    //> res15: scala.collection.immutable.Set[Int] = Set(1, 2)
  	a intersect b                             //> res16: scala.collection.immutable.Set[Int] = Set(5, 3, 4)
  	//set union
  	a | b                                     //> res17: scala.collection.immutable.Set[Int] = Set(5, 1, 6, 2, 7, 3, 4)
  	a diff b                                  //> res18: scala.collection.immutable.Set[Int] = Set(1, 2)
}