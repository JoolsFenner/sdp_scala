package revision

object KeySyntax {
  /**
  * apply method
  */
  val a = new MyInt(3)                            //> a  : revision.MyInt = revision.MyInt@694f9431
  a(3)                                            //> res0: Int = 6
  
  
  
  /**
  * for comprehensions
  */
  for(i <- 1 to 3; j <- 1 to 3 if i != j) yield(i + j)
                                                  //> res1: scala.collection.immutable.IndexedSeq[Int] = Vector(3, 4, 3, 5, 4, 5)
                                                  //| 
  
                                     
 /**
 * default args
 */
 def parenthesize(str: String, left: String = "[", right: String = "]"): String =
 	left+str+right                            //> parenthesize: (str: String, left: String, right: String)String
 	
 	parenthesize("hello")                     //> res2: String = [hello]
 	parenthesize("{", "yo", "}")              //> res3: String = yo{}
 	
 	
 	/**
 	*
 	* var args
 	*/
 	def myVargs(x: Int*):Int = {
 		var count = 0
 	for (arg <- x){
 		count += 1
 	}
 		count
 	}                                         //> myVargs: (x: Int*)Int
 	
 	myVargs(1,2,3,4,5,6)                      //> res4: Int = 6
 
 
 /**
 * Arrays and Arraybuffers
 */
 	 
 	 val b = new Array[Char](5)               //> b  : Array[Char] = Array( ,  ,  ,  ,  )
 	 for(i <- 0 to b.length -1) b(i) = 'a'
 	 b                                        //> res5: Array[Char] = Array(a, a, a, a, a)
 	 
 	 import scala.collection.mutable.ArrayBuffer
 	 val c = new ArrayBuffer[String]          //> c  : scala.collection.mutable.ArrayBuffer[String] = ArrayBuffer()
 	 c += "hello"                             //> res6: revision.KeySyntax.c.type = ArrayBuffer(hello)
   c ++= Array("how","is","it","going?")          //> res7: revision.KeySyntax.c.type = ArrayBuffer(hello, how, is, it, going?)
	 
	 val d = List(1,2,3,4,5,7,8,9,10)         //> d  : List[Int] = List(1, 2, 3, 4, 5, 7, 8, 9, 10)
	 d.filter(x => x % 2  == 0 ).map(y => y * 2)
                                                  //> res8: List[Int] = List(4, 8, 16, 20)
	//below is same as above
	d.filter(_ % 2 == 0).map(_ * 2)           //> res9: List[Int] = List(4, 8, 16, 20)
	
	
	/**
	*
	* Common algorithms
	*
	*/
	val e = ArrayBuffer(5,7,3,10,99,-1,1)     //> e  : scala.collection.mutable.ArrayBuffer[Int] = ArrayBuffer(5, 7, 3, 10, 9
                                                  //| 9, -1, 1)
	e.sortWith(_ < _)                         //> res10: scala.collection.mutable.ArrayBuffer[Int] = ArrayBuffer(-1, 1, 3, 5,
                                                  //|  7, 10, 99)

	val f = "hi my name is jools"             //> f  : String = hi my name is jools
	e.mkString(" and ")                       //> res11: String = 5 and 7 and 3 and 10 and 99 and -1 and 1
	
	
	


}


class MyInt(i:Int){
	def apply(x:Int):Int = x + i
}