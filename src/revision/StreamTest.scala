package revision

object StreamTest extends App {
  
  def iterate[T](x: T)(f: T => T): Stream[T] = {
    x #:: iterate(f(x))(f)
  }
  
  val powersOf2 = iterate(0)(x => x + 4)
  
  println(powersOf2.take(50) force)

}