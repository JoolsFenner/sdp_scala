package revision

object RevisionWorkSheet {
	println("hello")                          //> hello
	val y = List(1,2,3)                       //> y  : List[Int] = List(1, 2, 3)
	println("goodbye")                        //> goodbye
	
 	def loop: Boolean = loop                  //> loop: => Boolean
 
 	def x = loop                              //> x: => Boolean
 
 //Write a function and such that for all argument expressions x and y: and(x,y) == x && y
 
 def and(x:Boolean, y:Boolean) =
 	if(x) y else false                        //> and: (x: Boolean, y: Boolean)Boolean
  
  and(true,false)                                 //> res0: Boolean = false
  
}