package revision

object RevisionClass140515_c {
  
 //CURRYING
 
 def f(x:Int, y: Int) = {x + y}                   //> f: (x: Int, y: Int)Int
 f(3,4)                                           //> res0: Int = 7
 
 def ff(x:Int)(y:Int) = x + y                     //> ff: (x: Int)(y: Int)Int
 ff(3)(4)                                         //> res1: Int = 7
 
 val x = ff(3)_                                   //> x  : Int => Int = <function1>
 
 x                                                //> res2: Int => Int = <function1>
 x(4)                                             //> res3: Int = 7
 x(5)                                             //> res4: Int = 8
 
  
}