//package revision
////http://mauricio.github.io/2013/11/25/learning-scala-by-building-scala-lists.html
//object ListExample extends App {
//  
// val x = new StringBuilder()
// 
// x.append(1,2,3).toList
// println(x)
// 
//  val y = LinkedList(1,2,3)
//  //println(y)
//  //println(y.map(x => x * x))
//}
//
//
// //LinkedList implementation
//
//case class Node[+E]( val head : E, val tail : LinkedList[E]  ) extends LinkedList[E]
//case object Empty extends LinkedList[Nothing]
//
//sealed trait LinkedList[+E]{
//  
//  //map definition
//  def map[R]( f : E => R ) : LinkedList[R] = {
//    this match {
//      case Node(head, tail) => Node(f(head), tail.map(f))
//      case Empty => Empty
//    }
//  }
//
//  
//  def filter( f : (E) => Boolean ) : LinkedList[E] = {
//  foldRight(LinkedList[E]()) {
//    (item, acc) =>
//      if ( f(item) ) {
//        Node(item, acc)
//      } else {
//        acc
//      }
//  }
//}
//  
//  
//  //foldleft definition
//  final def foldLeft[B]( accumulator : B )( f : (B,E) => B ) : B = {
//    this match {
//      case Node(head, tail) => {
//        val current = f(accumulator, head)
//        tail.foldLeft(current)(f)
//      }
//      case Empty => accumulator
//    }
//  }
//  
//  def reverse() : LinkedList[E] = {
//  foldLeft(LinkedList[E]()) {
//    (acc, item) =>
//      Node(item, acc)
//    }
//  }
//  
//  def foldRight[B]( accumulator : B )(f : (E,B) => B) : B = {
//    reverse().foldLeft(accumulator)((acc,item) => f(item,acc))
//  }
//  
//  
//}
//
//
////companion obect
//object LinkedList {
//
//  def apply[E]( items : E* ) : LinkedList[E] = {
//    if (items.isEmpty) {
//      Empty
//    } else {
//      Node( items.head, apply(items.tail : _*) )
//    }
//  }
//
//}