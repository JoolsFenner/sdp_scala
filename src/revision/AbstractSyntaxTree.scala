package revision
/*
 * 1 -> Term(1)
 * 3 + 4 -> Func(plus,Seq(3,4)
 * 
 *          Func
 *         /    \
 *     plus     seq
 *             /   \
 *            3     4
 *            
 *     3 -4 + 1
 *     
 *     
 *       Func
 *      /    \
 *    minus   seq
 *           / | \
 *          3  4  Func 
 *               /    \
 *              plus   seq
 *                      |
 *                     Term(1) 
 *            
 */

sealed trait EvaluationTree
final case class Term(i:Int) extends EvaluationTree
final case class Func(s:String, args: Seq[EvaluationTree]) extends EvaluationTree


object Tester extends App{
  def print(e:EvaluationTree):String = e match{
    case Term(i) => i.toString
    case Func(s,args) => "(" + s + " " + printThing(args, " ") + ")" 
  }
  
  def printThing(args: Seq[EvaluationTree], sep:String):String = args match{
  
    case Nil => ""
    case s::Nil => print(s)
    case hd::tl => print(hd) + sep + printThing(tl,sep)
  }
  
  
  
 println(
     
     print(Func("*",List(Term(2),
                         Func("+", List(Term(100), Term(45))))))
 )
 

  
}

