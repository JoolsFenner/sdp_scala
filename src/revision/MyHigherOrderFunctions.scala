package revision

import scala.annotation.tailrec

object MyHigherOrderFunctions extends App {
  
  def myMap[T](f: T => T, lst: List[T]):List[T] = lst match{
    case Nil => Nil
    case hd :: tl => f(hd) :: myMap(f, tl) 
  }
  
  
  
  def myFilter[T](f: T => Boolean, lst: List[T]):List[T] = lst match{
    case Nil => Nil
    case hd :: tl => if(f(hd))
                      hd :: myFilter(f,tl)
                     else  
                       myFilter(f, tl.tail)                    
  }
  
  @tailrec
  def myReduce[A ,B](lst: Seq[A], acc: B)(f: (B, A) => B): B = lst match {
    case Seq() => acc
    case hd::tl => 
      myReduce(tl, f(acc, hd))(f)
  }

////http://stackoverflow.com/questions/24232235/implementation-of-foldleft-in-scala
//  def foldLeft[A, B](xs: Seq[A], z: B)(op: (B, A) => B): B = {
//  def f(xs: Seq[A], acc: B): B = xs match {
//    case Seq()   => acc
//    case x +: xs => f(xs, op(acc, x))
//  }
//  f(xs, z)
//}
  
  
  
 def myflatten[T](xs: List[T]): List[T] = xs match {
   case Nil => Nil
   case (hd: List[T]) :: tl => myflatten(hd) ++ myflatten(tl)
   case  hd::tl => hd :: myflatten(tl)
 }
  
 
 
 
  
	
  
  

 
 
 
 
 
  
  val numList = List(4,5,8,1,8,0)
  val nestedIntList = List(1, 2, List (3,4, List(5,6)),List(7,8))
  val stringList = List("apple", "Cow", "elephant","racoon", "zebra", "Aardvark")
  val nestedstringList = List("apple", List("Cow", "elephant","racoon", "zebra", List("cat", "dog"),"Aardvark"))
  val mixedList = List("a", 1, 2, "b", 19, 42.0, List(1)) //this is a List[Any]
  //println("myMap int: " + myMap[Int](x => x * x, numList))
  //println("myFilter string: " + myFilter[String](x => x(0).toString.equalsIgnoreCase("a"), stringList))
  ////note below doesn't work if using * as will times by zero in base case
  //println("myReduce int" + myReduce[Int, Int](numList, 1)((acc, x) => acc + x))
  
//  println(nestedstringList)
//  println(myflatten(nestedstringList))
  
//  val results = mixedList.collect {
//	  case s: String => "String:" + s
//	  case i: Int => "Int:" + i.toString
//	  case d: Double => "Double:"  + d.toString
//	  case l: List[_] => "List Int" + l.toString
//	}
//  println(results)

val myCollect =  mixedList.map{x => x match {
		 							case s: String => "String:" + s
			  						case i: Int => "Int:" + i.toString
			  						case d: Double => "Double:"  + d.toString
			  						case l: List[_] => "List" + l.toString}
  						}

  println(myCollect)


}


