package revision

object KeySyntax2 {
 
 /**
 * Maps
 */
 
 val a = Map("Julian" -> 33, "Linda" -> 32, "Andrea" -> 36)
                                                  //> a  : scala.collection.immutable.Map[String,Int] = Map(Julian -> 33, Linda ->
                                                  //|  32, Andrea -> 36)
 a.get("Julian")                                  //> res0: Option[Int] = Some(33)
 
 for((key, value) <- a) println("key " + key + " value " + value)
                                                  //> key Julian value 33
                                                  //| key Linda value 32
                                                  //| key Andrea value 36
 
 
}