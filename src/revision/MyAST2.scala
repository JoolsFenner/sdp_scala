package revision

sealed trait EvaluationList
final case class Term3(n: Int) extends EvaluationList
final case class Func3(str: String, args: List[EvaluationList]) extends EvaluationList

object MyEvaluationTreeList extends App {

  def eval(el: EvaluationList): Int = el match {
    case Term3(n) => n
    //case Sum(n1, n2) => eval(n1) + eval(n2)
    //case Minus(n1, n2) => eval(n1) - eval(n2)
    case Func3(str, args) => str match {
      case "+" => (args map (x=> eval(x))).fold(0)(_+_)
      case "-" => (args map (x=> eval(x))).fold(0)(_-_)
      //case "/" => (args map (x=> eval(x))).fold(0)(_/_)
      case "*" => (args map (x=> eval(x))).fold(1)(_*_)
      //case _ => sys.error("Unsupported function.")
    }
  }

 println(eval(Func3("+", List(Term3(2), Func3("-", List(Term3(3), Term3(5), Term3(6)))))))
 
 println(eval(Func3("", List(Term3(10), Term3(3)))))
 
 
}