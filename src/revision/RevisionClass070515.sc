package revision

object RevisionClass070515 {
  
  ((x: Int) => x * x)(4)                          //> res0: Int = 16
  val x = ((x: Int) => x * x)                     //> x  : Int => Int = <function1>
  x(3)                                            //> res1: Int = 9
  
  
  def add(x:Int, y:Int) = x + y                   //> add: (x: Int, y: Int)Int
  add(3,4)                                        //> res2: Int = 7
  
  
  def addCurry(x:Int) (y:Int) = x + y             //> addCurry: (x: Int)(y: Int)Int
  
  //not giving second argument
  val add3 = addCurry(3)_                         //> add3  : Int => Int = <function1>
  add3(5)                                         //> res3: Int = 8
  
  
  //[[1,2],[3,4] -> [1,2,3,4]
  
  def flatten(ls: Seq[Seq[Int]]): Seq[Int] = ls match{
  	case Nil => Nil
  	// if was deeply nested would need something like this....
  	//case hd::Nil => //if hd is list etc....
  	case hd::tl => hd ++ flatten(tl)
  
  }                                               //> flatten: (ls: Seq[Seq[Int]])Seq[Int]
  
  // multiplyList(List(3,4,5),2.0) -> List(6,8,10)
  def multiplyList(xs: List[Double], value: Double): List[Double] = xs match{
  	case Nil => Nil
  	case hd::tl => hd * value :: multiplyList(tl,value)
  }                                               //> multiplyList: (xs: List[Double], value: Double)List[Double]
  
  def multiplyList2(xs: List[Double], value: Double): List[Double] =
  xs.map(x => x * value)                          //> multiplyList2: (xs: List[Double], value: Double)List[Double]
	
	
	
	
	
	
}


//class MyList[T]{

	x`
		
//}