package revision

object LinkedListTester extends App {
  val jList = Node(30, Empty)
  val jList2 = jList.setHead(8)
  
  //println(jList2.foldLeft(1)((acc,el) => el * acc))
  
  val longList = Node(1,Node(2,Node(3, Node(4,Node(5,Empty)))))
  
  //print(longList.dropWhile { x => x < 4 }.toList)
  
  println(longList ++ jList)
}


final case class Node[+A](head: A, tail: LinkedList[A]) extends LinkedList[A] {

  val size = 1 + tail.size

  val isEmpty: Boolean = false
}

case object Empty extends LinkedList[Nothing] {
  val size = 0

  val isEmpty: Boolean = true

  override def head: Nothing =  
    throw new NoSuchElementException("head of empty list")

  override def tail: LinkedList[Nothing] =
    throw new UnsupportedOperationException("tail of empty list")
}

sealed trait LinkedList[+A] {

  def size: Int

  def isEmpty: Boolean

  def tail: LinkedList[A]

  def head: A

  def toList: List[A] = this match {
    case Empty => List()
    case Node(h, tl) => h :: tl.toList
  }

  def setHead[B >: A](v: B): LinkedList[B] = {
    Node(v, this)
  }

  def ::[B >: A](element: B): LinkedList[B] = {
    Node(element, this)
  }

  def dropWhile(f: A => Boolean): LinkedList[A] = this match {
    case Empty => this
    case Node(h,tl) => if(f(h)) 
                          tl.dropWhile(f)
                        else
                          this  
  }

  def ++[B >: A](that: LinkedList[B]): LinkedList[B] = this match {
    case Empty => that
    case Node(h,tl) => h :: tl ++ that
  }

  def foldLeft[B](acc: B)(f: (B, A) => B): B = this match {
    case Empty => acc
    case Node(h,tl) => tl.foldLeft(f(acc,h))(f)
  }

  def fold[B >: A](acc: B)(f: (B, B) => B): B = this match {
    case Empty => acc
    case Node(h,tl) => tl.fold(f(acc,h))(f)
  }

  // This works because foldLeft goes from left to right and lists defined from right to left Node(1,Node(2, Node(3,Empty))) <-- empty is first then we have the objects in terms of eval
  //http://mauricio.github.io/2013/12/08/learning-scala-by-building-scala-lists-part-2.html
  def reverse(): LinkedList[A] = 
   foldLeft(LinkedList[A]())((acc, item) => item :: acc)
  

  def foldRight[B](acc: B)(f: (A, B) => B): B = ???

  def reduce[B >: A](f: (B, B) => B): B = ???

  def filter(p: A => Boolean): LinkedList[A] = ???

  def find(p: (A) => Boolean): Option[A] = ???

  def map[B](f: A => B): LinkedList[B] = ???

  def flatten[B]: LinkedList[B] = ???

  def flatMap[B](f: A => LinkedList[B]): LinkedList[B] = ???

  def negate[B >: A](predicate: B => Boolean): B => Boolean = ???

  def partition(p: (A) => Boolean): (LinkedList[A], LinkedList[A]) = ???

  // Manually test
  def foreach(f: A => Unit): Unit = ???

  def zip[B](that: LinkedList[B]): LinkedList[(A, B)] = ???

  def collect[B](pf: PartialFunction[A, B]): LinkedList[B] = ???

  // use an immutable map
  def groupBy[K](f: (A) => K): Map[K, LinkedList[A]] = ???

  def splitAt(n: Int): (LinkedList[A], LinkedList[A]) = ???

  def take(n: Int): LinkedList[A] = ???

  def drop(n: Int): LinkedList[A] = ???

  def last: A = ???

  def +:[B >: A](elem: B): LinkedList[B] = ???

  def :::[B >: A](prefix: LinkedList[B]): LinkedList[B] = ???

  def sum: A = ???

  def max: A = ???

  def min: A = ???

  def mergeSort(): LinkedList[Int] = ???
}

object LinkedList {
  def apply[A](items: A*): LinkedList[A] =
    items.foldRight(Empty: LinkedList[A])(Node(_, _))
}