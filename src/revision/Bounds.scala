package revision


class Container[A <% Int] { def addIt(x: A) = 123 + x }

object Bounds extends App {

  implicit def strToInt(x: String) = x.toInt
  //implicit def strToInt2(x: String) = x.toDouble
  
  (new Container[String]).addIt("123")
  
  (new Container[String]).addIt("123.2")
}