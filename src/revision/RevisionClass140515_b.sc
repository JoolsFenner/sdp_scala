package revision

object RevisionClass140515_b {
  
  import scala.annotation.tailrec
  
  def factorial(n: Double): Double =
	factorial(n, 1)

	@tailrec
	def factorial(n: Double, acc: Double): Double = {
		if(n <= 0)
			acc
		else
			factorial(n-1, acc * n)
	}
	
	factorial(4)
	
	
	
	@tailrec
	def factorial2(n: Double, acc: Double = 1): Double = {
		if(n <= 0)
			acc
		else
			factorial2(n-1, acc * n)
	}
	
	
	
	
}