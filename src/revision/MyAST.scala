package revision


sealed trait MyEvalTree
final case class MyTerm(i:Int) extends MyEvalTree
final case class MyFunc(s:String, args: Seq[MyEvalTree]) extends MyEvalTree


object MyAST extends App{
  
  def print(evaltree: MyEvalTree):String = evaltree match {
    case MyTerm(i) => i.toString + " "
    case MyFunc(s, args) => "(" + s + " " + printFunc(args, ",") + ")" 
  }

  def printFunc(x: Seq[MyEvalTree], sep: String):String = x match {
    case Nil => ""  
    case hd::Nil => print(hd)
    case hd::tl  => print(hd) + printFunc(tl, sep)
    
  }
  
  println(
		  print(
		      MyFunc("+", 
		          List(
		              MyFunc("/", List(MyTerm(100),MyTerm(3))), 
		              MyFunc("*", List(MyTerm(20),MyTerm(9)))
				  )))
   )
  
  //println(print(MyFunc("+", List(MyTerm(1)))))
  
}