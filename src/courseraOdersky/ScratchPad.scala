package courseraOdersky

class ScratchPad {
  def loop: Boolean = loop                  //> loop: => Boolean
 
  def a = loop                              //> x: => Boolean
  //val b = loop //infinite loop
  
 //Write a function and such that for all argument expressions x and y: and(x,y) == x && y
 
 def and(x:Boolean, y:Boolean) =
  if(x) y else false                        //> and: (x: Boolean, y: Boolean)Boolean

  and(true,false)                                 //> res0: Boolean = false
  
  def and(x:Boolean, y: => Boolean)=
    if (x) y else false
    
  and(true,false)
  
  
}