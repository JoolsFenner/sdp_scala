package courseraOdersky

abstract class IntSet {
  def incl(c: Int): IntSet
  def contains(x: Int): Boolean
  
  
    
  
}

class Empty extends IntSet{
  
  def contains(x:Int):Boolean = false
  
  def incl(x:Int):IntSet = new NonEmpty(x, new Empty, new Empty)
  
   
}

class NonEmpty(elem:Int, left: IntSet, right: IntSet) extends IntSet{
  def contains(x: Int): Boolean =
      if (x < elem) left contains x
      else if (x > elem) right contains x
      else true
  
  def incl(x: Int): IntSet =
    if (x < elem) new NonEmpty(elem, left incl x, right)
    else if (x > elem) new NonEmpty(elem, left, right incl x)
    else this
  
}

object tester extends App{
  
  val tree = new Empty
  tree.incl(10)
  tree.incl(4)
  tree.incl(3)
  tree.incl(12)
  
  println(tree.contains(3))
  
  
}