package courseraOdersky

object Lecture4_1_FunctionsasObjects {
  val x = new MyInteger(3)                        //> x  : courseraOdersky.MyInteger = courseraOdersky.MyInteger@694f9431
  x + 4                                           //> res0: Int = 7
  x + 4.0                                         //> res1: Double = 7.0
}



// A => B is abbreviation for...
trait Function[A,B]{
	def apply(a: A):B
}


class MyInteger(i: Int){
	
	def + (that: Int): Int = this.i + that
	def + (that: Double): Double = this.i + that
	
}