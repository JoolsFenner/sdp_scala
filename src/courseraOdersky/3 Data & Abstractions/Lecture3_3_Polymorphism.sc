package courseraOdersky

object Lecture3_3_Polymorphism {
	
	val list = new Cons(1,
								new Cons(2,
									new Cons(3, new Nil)))
                                                  //> list  : courseraOdersky.Cons[Int] = courseraOdersky.Cons@3581c5f3
	
	list.tail.tail.head                       //> res0: Int = 3
	
	val list2 = new Cons("a",
								new Cons("b",
									new Cons("c", new Nil)))
                                                  //> list2  : courseraOdersky.Cons[String] = courseraOdersky.Cons@7f63425a
	
	list2.tail.tail.head                      //> res1: String = c
}


trait List[T] {
	def isEmpty: Boolean
	def head: T
	def tail: List[T]
}

class Cons[T](val head: T, val tail: List[T]) extends List[T] {
	def isEmpty = false
}

class Nil[T] extends List[T]{
	def isEmpty = true
	def head = throw new NoSuchElementException("Nil.head")
	def tail = throw new NoSuchElementException("Nil.tail")
	
}