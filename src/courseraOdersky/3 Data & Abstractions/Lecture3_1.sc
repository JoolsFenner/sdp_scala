package courseraOdersky

// class hierarchies

object intsets {

	val t1 = new NonEmpty(3, Empty, Empty)    //> t1  : courseraOdersky.NonEmpty = {.3.}
	val t2 = t1 incl 4                        //> t2  : courseraOdersky.IntSet = {.3{.4.}}
	
		t1 union t2                       //> res0: courseraOdersky.IntSet = {.3{.4.}}
}

abstract class IntSet {

	def incl(x:Int):IntSet
	def contains(x:Int):Boolean
	def union(other:IntSet):IntSet

}

//sets as binary trees
//invariant is that nodes on right are higher than nodes on the left


object Empty extends IntSet{
	def contains(x:Int):Boolean = false
	def incl(x: Int): IntSet = new NonEmpty(x, Empty, Empty)
	override def toString = "."
	
	def union(other:IntSet):IntSet = other
	
}


class NonEmpty(elem: Int, left: IntSet, right: IntSet) extends IntSet {
	
	def contains(x: Int): Boolean =
		if (x < elem) left contains x
		else if (x > elem) right contains x
		else true
	
	def incl(x: Int): IntSet =
		if (x < elem) new NonEmpty(elem, left incl x, right)
		else if (x > elem) new NonEmpty(elem, left, right incl x)
	else this
	
	def union(other:IntSet):IntSet =
		((left union right) union other) incl elem
	
	
	
	override def toString ="{" + left + elem + right + "}"
	
	}