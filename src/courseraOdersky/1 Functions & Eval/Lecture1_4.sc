package courseraOdersky

//Lecture 1.4 Conditionals with Value Definition

object Lecture1_4 {
  println("Welcome to the Scala worksheet")
  
   	def loop: Boolean = loop
 
 		def x = loop
 		//Write a function and such that for all argument expressions x and y: and(x,y) == x && y
 		def and(x:Boolean, y:Boolean) =
 			if(x) y else false
 
// 		and(false,loop) // infinite loop
  	
  	def andCBN(x:Boolean, y: => Boolean) =
  		if(x) y else false
 
 		println("hello")
  	
  	andCBN(false,loop)
}