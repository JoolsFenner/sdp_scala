package courseraOdersky

//Tail Recursion

object Lecture1_7 {
	
	//@tailrec
  def gcd(a: Int, b: Int): Int =
		if (b == 0) a else gcd(b, a % b)  //> gcd: (a: Int, b: Int)Int
	/*
		gcd(14, 21) is evaluated as follows:
		gcd(14, 21)
		! if (21 == 0) 14 else gcd(21, 14 % 21)
		! if (false) 14 else gcd(21, 14 % 21)
		! gcd(21, 14 % 21)
		! gcd(21, 14)
		! if (14 == 0) 21 else gcd(14, 21 % 14)
		!! gcd(14, 7)
		!! gcd(7, 0)
		! if (0 == 0) 7 else gcd(0, 7 % 0)
		! 7
*/

	def factorial(n: Int): Int =
		if (n == 0) 1 else n * factorial(n - 1)
                                                  //> factorial: (n: Int)Int


	/*
	NOT TAIL RECURSIVE, See how the final call keeps growing
	! if (4 == 0) 1 else 4 * factorial(4 - 1)
	!! 4 * factorial(3)
	!! 4 * (3 * factorial(2))
	!! 4 * (3 * (2 * factorial(1)))
	!! 4 * (3 * (2 * (1 * factorial(0)))
	!! 4 * (3 * (2 * (1 * 1)))
	!! 120
	
	*/
	
	def factorialTR(n: Int):Int = {
		def factorialAcc(n:Int, acc:Int):Int = {
			if (n == 0) acc else factorialAcc(n - 1, n * acc)
		}
		
		factorialAcc(n,1)
	}                                         //> factorialTR: (n: Int)Int
	
	factorial(6)                              //> res0: Int = 720
	factorialTR(6)                            //> res1: Int = 720
	
}