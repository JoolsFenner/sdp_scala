package courseraOdersky

//Blocks and lexical scope

object Lecture1_6 {

	//refactored from previous page
	
	def sqrt(x:Double) = {
	
		def sqrtIter(guess:Double, x:Double):Double =
	  	if(isGoodEnough(guess,x))guess
	  	else sqrtIter(improve(guess,x),x)
	  		  	
	  	def isGoodEnough(guess:Double, x:Double): Boolean =
	  		Math.abs(guess * guess - x) < 0.001
	
			def improve(guess:Double, x: Double) =
				(guess + x / guess) / 2
				
			sqrtIter(1.0, x)
			
	}                                         //> sqrt: (x: Double)Double
		
		sqrt(2)                           //> res0: Double = 1.4142156862745097
		sqrt(4)                           //> res1: Double = 2.0000000929222947

	
	
	
	val x = 0                                 //> x  : Int = 0
	def f(y: Int) = y + 1                     //> f: (y: Int)Int
		val result = {
		val x = f(3)
		x * x // this is inner block's x
	}                                         //> result  : Int = 16

// Can use above idea to remove redundant xs in the top function
// Nesting thing isn't just namspace control
// also reusing outer defintiions without re-passing them in inner functions

	def sqrt2(x:Double) = {
	
		def sqrtIter(guess:Double):Double =
	  	if(isGoodEnough(guess))guess
	  	else sqrtIter(improve(guess))
	  		  	
	  	def isGoodEnough(guess:Double): Boolean =
	  		Math.abs(guess * guess - x) < 0.001
	
			def improve(guess:Double) =
				(guess + x / guess) / 2
				
			sqrtIter(1.0)
			
	}                                         //> sqrt2: (x: Double)Double


}