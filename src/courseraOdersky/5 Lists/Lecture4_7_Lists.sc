package courseraOdersky

object Lecture4_7_Lists {
  /**
  * Constructors of Lists
  * Cons
  */
  
  //NOT ALLOWED
  //val fruit = "apples" :: "oranges"
  val fruit = "apples" :: Nil                     //> fruit  : List[String] = List(apples)
  val fruit2 = "apples" :: ("oranges" :: Nil)     //> fruit2  : List[String] = List(apples, oranges)
  val fruit3 = "apples" :: ("oranges" :: ("pears" ::Nil))
                                                  //> fruit3  : List[String] = List(apples, oranges, pears)
                
  // NOT ALLOWED
  //val nums = 1 :: 2 :: 3
  val nums = 1 :: 2 :: 3 :: Nil                   //> nums  : List[Int] = List(1, 2, 3)
  
  /**
  *Concatenation
  *
  */
  val a = List(1,2,3,4)                           //> a  : List[Int] = List(1, 2, 3, 4)
  val b = List(5,6,7,8)                           //> b  : List[Int] = List(5, 6, 7, 8)
  a ::: b                                         //> res0: List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8)
  //NOTE DIFFERENCE
  a :: b                                          //> res1: List[Any] = List(List(1, 2, 3, 4), 5, 6, 7, 8)
  
  /**
  *
  *Insert sort
  */
  def isort(xs: List[Int]): List[Int]  = xs match {
		case List() => List()
		case y :: ys => insert(y, isort(ys))
	}                                         //> isort: (xs: List[Int])List[Int]

	def insert(x: Int, xs: List[Int]): List[Int] = xs match{
		case List() => List(x)
		case y :: ys => if (x < y)
											x :: xs
										else
											y :: insert(x,ys)
										
	}                                         //> insert: (x: Int, xs: List[Int])List[Int]
  
  
  val x = List(2,1,99,-3,999,5,66,363)            //> x  : List[Int] = List(2, 1, 99, -3, 999, 5, 66, 363)
  isort(x)                                        //> res2: List[Int] = List(-3, 1, 2, 5, 66, 99, 363, 999)
  
 
 def reverse[T](xs: List[T]): List[T] = xs match {
 	case List() => xs
 	case y :: ys => reverse(ys) ::: List(y) //could use ++ as operator here
 		
 }                                                //> reverse: [T](xs: List[T])List[T]
 reverse(List(1,2,3,4,5,6))                       //> res3: List[Int] = List(6, 5, 4, 3, 2, 1)
  
 List(1,2) ::: List(3,4)                          //> res4: List[Int] = List(1, 2, 3, 4)
  
  
}