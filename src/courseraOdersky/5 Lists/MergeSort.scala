package courseraOdersky

object MergeSort extends App {
   
 def msort(lst: List[Int]): List[Int] = {
   val n = lst.length / 2
   if(n == 0) // lst is empty or of length 1
      lst
   else
   {
     val(lstA, lstB) = lst.splitAt(n)
     merge(msort(lstA), msort(lstB))
   }
 } 
 
 /**
  * Merge version 1. Less elegant
  */
//  def merge(a: List[Int], b:List[Int]): List[Int] = 
//    a match {  
//      case Nil => 
//        b
//      case ahd::atl  => 
//        b match {
//          case Nil => a
//          case bhd::btl => 
//            if(ahd < bhd) 
//              ahd :: merge(atl, b)
//            else
//               bhd :: merge(a, btl)
//      } 
//    }
    
 
 /**
  * Merge version 2.  More elegant, readable and closer to nature algorithm
  */
  def merge(a: List[Int], b:List[Int]): List[Int] = {
    (a, b) match {
      case (a, Nil) => a
      case (Nil,b)  => b
      // note even better to use case(xHd:Xtl, yHd, yTl) here...
      case (x,y)    => if(x.head < y.head)
                         x.head :: merge(a.tail,b)
                       else
                         y.head :: merge (a, b.tail) }
   }
  
  
    
  println(msort(List(4,6,71,7,24323,6,1,-1)))
    
  }