package courseraOdersky

object MergeSortGeneral extends App {

  def msort[T](lst: List[T])(f: (T,T)=> Boolean): List[T] = {
   val n = lst.length / 2
   if(n == 0) // lst is empty or of length 1
      lst
   else
   {
       def merge(a: List[T], b:List[T]): List[T] = {
        (a, b) match {
           case (a, Nil) => a
           case (Nil,b)  => b
           case (xHd::xTl, yHd::yTl) => 
             if( f(xHd,yHd) )
               xHd :: merge(xTl,b)
                 else
             yHd :: merge (a, yTl) 
          }
       } 
     
     val(lstA, lstB) = lst.splitAt(n)
     merge(msort(lstA)(f), msort(lstB)(f))
    } 
 } 
  
 
val x = msort(List(5,6,7,1,8,0,-99,1000)) ((x,y) => x > y)
println(x)

val y = msort(List("dog", "apple", "cat")) ((x,y) => x.head < y.head )
println(y) 


}