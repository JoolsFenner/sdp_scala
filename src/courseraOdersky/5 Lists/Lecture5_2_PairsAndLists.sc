package courseraOdersky

object Lecture5_2_PairsAndLists {
  
  val x = List(4,1,6,2,7,10)                      //> x  : List[Int] = List(4, 1, 6, 2, 7, 10)
  val n = x.length / 2                            //> n  : Int = 3
  val(a, b) = x.splitAt(n)                        //> a  : List[Int] = List(4, 1, 6)
                                                  //| b  : List[Int] = List(2, 7, 10)
  
  (a,b)                                           //> res0: (List[Int], List[Int]) = (List(4, 1, 6),List(2, 7, 10))
  
  val c = (1,2)                                   //> c  : (Int, Int) = (1,2)
  c._1                                            //> res1: Int = 1
  c._2                                            //> res2: Int = 2
  
}