package courseraOdersky


//Currying

object Lecture2_2 {

def sum(f: Int => Int): (Int, Int) => Int = {
	
	def sumF(a: Int, b: Int): Int =
		if (a > b) 0
		else f(a) + sumF(a + 1, b)
	sumF(1,2)
	sumF
	}                                         //> sum: (f: Int => Int)(Int, Int) => Int
 
 
 def sumF(a: Int, b: Int): Int =
		if (a > b) 0
		else sumF(a + 1, b)               //> sumF: (a: Int, b: Int)Int
 
 
 
 
 sum(x => x * x * x)                              //> res0: (Int, Int) => Int = <function2>
 sum(x => x * x * x)  (1,10)                      //> res1: Int = 3025
 def cube(x:Int) = x * x * x                      //> cube: (x: Int)Int
 sum(cube) (1,10)                                 //> res2: Int = 3025
 
 
 
 //this is equivalent
 def sum2(f: Int => Int)(a: Int, b: Int): Int =
	if (a > b) 0 else f(a) + sum2(f)(a + 1, b)//> sum2: (f: Int => Int)(a: Int, b: Int)Int
 
 	 sum2(x => x * x * x)  (1,10)             //> res3: Int = 3025
  
	 sum2(x => x * x * x)  (_,_)              //> res4: (Int, Int) => Int = <function2>

	 def sum2b = sum2(x => x * x * x)  (_,_)  //> sum2b: => (Int, Int) => Int

 	sum2b(1,10)                               //> res5: Int = 3025
 
 
 
 
 //Product functions that calcs product of the values of a function on a given inteval
 
 def product(f:Int => Int) (a:Int, b:Int):Int =
 	if(a > b) 1
 	else f(a) * product(f)(a + 1, b)          //> product: (f: Int => Int)(a: Int, b: Int)Int
 
 product(x => x * x) (3,4)                        //> res6: Int = 144
 

 def fact(n: Int) = product(x => x)(1,n)          //> fact: (n: Int)Int
 
 fact(5)                                          //> res7: Int = 120
 
 
 //generalise sum and product
 // the mapping is common f: Int => Int
 // the two bounds are common a and b
 // the two things that differ are the unit value and combining function
 // we want version of map reduce that maps values in the internal to new values
 // and reduce them
 // combine is for sum or product, zero is for 0 or 1

 	def mapReduce(f: Int => Int, combine: (Int,Int) => Int, zero: Int)(a:Int, b:Int) : Int = {
 		if(a > b) zero
 		else combine(f(a), mapReduce(f, combine,zero)(a+1, b))
 	}                                         //> mapReduce: (f: Int => Int, combine: (Int, Int) => Int, zero: Int)(a: Int, b
                                                  //| : Int)Int
 
 	def product2(f: Int => Int) (a: Int, b: Int): Int = mapReduce(f, (x, y) => x * y, 1)(a,b)
                                                  //> product2: (f: Int => Int)(a: Int, b: Int)Int
                                                  
                                                  
	product(x => x * x)(3,4)                  //> res8: Int = 144
	def fact2(n:Int) = product(x => x)(1,n)   //> fact2: (n: Int)Int
	fact(5)                                   //> res9: Int = 120
 
 	def curryTest(x: Int)(y:Int)(z:  Int): Int = x + y + z
                                                  //> curryTest: (x: Int)(y: Int)(z: Int)Int
 
  curryTest(1)_                                   //> res10: Int => (Int => Int) = <function1>
 	curryTest(1)(2)_                          //> res11: Int => Int = <function1>
  curryTest(1)(2)(3)                              //> res12: Int = 6
}