package courseraOdersky

//Higher Order Functions

object Lecture2_1 {


  def sum(f: Int => Int, a: Int, b: Int): Int = {
        def loop(a: Int, acc: Int): Int = {
          if (a > b) acc
          else loop(a + 1, f(a) + acc)
        }
        loop(a, 0)
      }                                           //> sum: (f: Int => Int, a: Int, b: Int)Int
  
      sum(x => x, 3, 5)                           //> res0: Int = 12
      sum(x => x * x, 3, 5)                       //> res1: Int = 50
      sum(x => x * x * x, 3, 6)                   //> res2: Int = 432
}