package courseraOdersky

object Lecture2_2_Exercises {
  
  def product(f: Int => Int)(a: Int, b: Int): Int = {
  	if(a == b)
  		b
  	else
  	f(a) * product(f)(a + 1, b)
  }
  
  //Teach number in range a -> b squared and multiplied
  product(x => x )(1, 5)
  
  // factorial defined in terms of product
  def fact(n: Int) = product(x => x)(1,n)
  fact(5)

  
  /*
  Generalisiation of sum and product
  COMMONALITIES = mapping ie f: Int => Int
  								the two sounds
  DIFFERENCES =   Unit value
  							  combining function
  
  need a version of map reduce, i.e maps values in interval and reduces them
  */
  
  def mapReduce(f: Int => Int, combine: (Int, Int) => Int, zero: Int)(a: Int, b: Int): Int =
  	if(a > b) zero
  	else combine( f(a), mapReduce(f, combine, zero) (a + 1, b) )


	def product2(n: Int) = mapReduce( (x: Int) => x, (a: Int, b:Int) => a * b, 1) (1,n)
	
	product2(5)
  
  
  //or
  
  def product3(f: Int => Int)(a: Int, b: Int): Int = mapReduce(f, (x, y) => x * y, 1)(a, b)
                                                  
 product3((x: Int) => x)(1,5)
}