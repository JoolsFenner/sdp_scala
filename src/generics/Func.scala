package generics

object Func extends App{
  // partial application of a function, return type is a function that returns C
  // Very common pattern known as Currying
  // Good way of carrying around values doing same thing as default argument
  def part[A,B,C](a:A, fn: (A,B) => C): (B => C) = 
    (b:B) => fn(a,b)
  
    
  def compose[A, B ,C ]  (f: B => C, g: A => B): A => C = 
    a => f(g(a))
  
  
  val a = 3
  val add3 = part(a, (a:Int,b:Int) => a +b )
  val add6 = part(a, (a:Int,b:Int) => a +b )
  //println(add3(6))
  //println(add6(6))
    
  
  
  val double =  (x: Double) => 2.0 * x
  val squared = (x: Double) => x * x
  
  println(compose(double,squared)(3))
  
}