package generics
import scala.annotation.tailrec
/*
 * T extends xxxx //     (subclass)
 * T extends ?    // <:  (upper bound)
 * T super ?      // >:   (lower bound)
 *
 * Student extends Person
 * Pair[Student] <=> Pair[Person]
 */

object Polymorphic extends App{


  def findFirst[T](as: Array[T], value: T): Int = {
   @tailrec
    def helper(n: Int): Int = 
    
      if(n >= as.length) -1
      else if (as(n) == value) n
      else helper(n + 1)
      
    helper(0)
    
  }
  
  def isSorted[T <% Comparable[T]] (as: Array[T], gt: (T,T) => Boolean): Boolean = {
    @tailrec
    def helper(n: Int): Boolean = 
    	if(n >= as.length -1) true	
    	else if(gt (as(n),as(n+1))) false
    	else(helper(n+1))
    helper(0)
    
  }
  
  
  
//  println(findFirst(Array("1","2","6","4"),"6"))
//  println(findFirst(Array(1,2,6,4),6))
  
  println( isSorted(Array("a","b","x","d"), (x:String,y:String) => x > y) )
  
  
  
  println( isSorted(Array(1,2,3,4), (x:Int,y:Int) => x > y) )  
  /*can't write this when ---> def isSorted[T <: Comparable[T]]  because int doesn't implement comparable
  * if you use [T <% Comparable[T]] then it will follow the "Implicit" chain
  */
  
  
  
  
}