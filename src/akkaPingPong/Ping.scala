//package akkaPingPong
//
//import akka.actor.Actor
//import akka.actor.ActorSystem
//import akka.actor.ActorRef
//import akka.actor.Props
//
//case object PingMessage
//case object PongMessage
//case object StartMessage
//case object StopMessage
//
//
////have to pass in a pong: ActorRef otherwise it won't know what pong is
//class Ping(pong: ActorRef) extends Actor { 
//  var count = 0
//  def incAndPrint: Unit ={
//    count += 1
//    println("Ping")
//    
//  }
//  
//  override def receive = {
//    case StartMessage =>
//      incAndPrint
//      pong ! PingMessage
//    case PongMessage =>
//      incAndPrint
//      if(count < 100)
//        sender ! PingMessage
//        else{
//          sender ! StopMessage
//          println("ping stopping")
//          context.stop(self)
//        }
//    case _ => println("can't understand message")
//  }
//
//  
//}
//
//
//
//class Pong extends Actor {
//  
//    override def receive = {
//    case PingMessage => 
//      println("\tPong")
//      sender ! PongMessage //sender property from Actor, i.e person who sent message
//    case StopMessage =>
//      println("\npong stopping")
//      context.stop(self)
//    case _ => println("don't understand message")
//  }
//  
//}
//
//object PingPong extends App {
//  val system = ActorSystem("PingPongSystem")
//  val pong = system.actorOf(Props[Pong], name = "pongactor")
//  val ping = system.actorOf(Props(new Ping(pong)), name = "pingactor")
//  //START
//  ping ! StartMessage
//  
//}