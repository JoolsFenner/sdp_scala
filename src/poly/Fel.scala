package poly

//Lion, Tiger, Panther, Cat

sealed trait Feline {
  def dinner: Food
} 

final case class Lion() extends Feline{
  def dinner: Food = Antelope
}

final case class Tiger() extends Feline{
  def dinner: Food = TigerStuff
}

final case class Panther() extends Feline{
  def dinner: Food = TigerStuff
}


//final case class Cat(favFood: String) extends Feline{
//  def dinner: Food = CatFood(favFood)
//}


sealed trait Food
final object Antelope extends Food
final object TigerStuff extends Food
final case class CatFood(food: Food) extends Food






object Test{
  def main(args: Array[String]){
    
    val jools = Tiger
    println(jools.getClass)
  }
  
  
}