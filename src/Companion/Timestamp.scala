package Companion

/*
 * Companion object syntax
 * Effectively replacement for static methods that you get in Java
 */

class Timestamp (val seconds: Long)

//overloadeded constructor in companion object
//note no val in object below
object Timestamp{
  def apply(hours: Int, minutes: Int, seconds:Int) = 
    new Timestamp(hours * 60* 60 + minutes * 60 + seconds)

}
