package Companion

class Person(val firstName: String, val lastName: String) 

object Person{
  def apply(fullName: String) ={ 
    val x = fullName.split(" ")
    new Person(x(0), x(1))
  }

}


// add companion object for person that takes single string and 
//splits it into the separate firstName lastName components